import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.util.JSON;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.hamcrest.core.Is;
import org.junit.*;

public class MongoDBTest {
    static MongoClientURI mongoConnectionString;
    static MongoClient mongoClient;
    static MongoDatabase database;
    static MongoCollection<Document> collection;
    private static final int Port = 27017;

    @BeforeClass
    public static void initConnection() {
        mongoConnectionString = new MongoClientURI("mongodb://localhost:" + Port);
        mongoClient = new MongoClient(mongoConnectionString);
    }

    @Before
    public void someInitCommandsToUseLater() {
        database = mongoClient.getDatabase("4AHIF_DBI_Database_Happy");
        collection = database.getCollection("4AHIF_DBI_Collection");
        Document document = new Document()
                .append("Vorname", "Pascal")
                .append("Nachname", "Happy");
        collection.insertOne(document);
    }

    @Test
    public void checkIfRedisisAvailable() {

    }

    @Test
    public void getAttributesOfFirstDocument() {
        Assert.assertThat(collection.find().first().get("Vorname").toString(), Is.is("Pascal"));
        Assert.assertThat(collection.find().first().get("Nachname").toString(), Is.is("Happy"));
    }

    @Test
    public void findDocumentCalledHappy() {
        Document findDocWithElmoghazi = collection.find(Filters.eq("Nachname", "Happy")).first();
        Assert.assertNotNull(findDocWithElmoghazi);
    }

    @Test
    public void removeDocumentAndCheckIfItStillExists() {
        collection.deleteMany(Filters.eq("Vorname", ""));
        Document findDoc = collection.find(Filters.eq("Vorname", "Pascal")).first();
        Assert.assertNull(findDoc);
    }


    @AfterClass
    public static void clientShutdown() {
        mongoClient.close();

    }
}
