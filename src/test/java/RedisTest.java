import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import org.assertj.core.api.Assertions;
import org.hamcrest.core.Is;
import org.junit.*;

import java.util.*;

public class RedisTest {

    static RedisClient redisClient;
    static StatefulRedisConnection<String, String> redisConnection;
    static RedisCommands<String, String> syncRedisCommands;
    private static final int port = 6379;


    @BeforeClass
    public static void initConnection() {
        redisClient = RedisClient.create("redis://localhost:" + port + "/0");
        try {
            redisConnection = redisClient.connect();
            syncRedisCommands = redisConnection.sync();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @BeforeClass
    public static void reset() {
        syncRedisCommands.del("mylist");
        syncRedisCommands.del("Spengergasse");
        syncRedisCommands.del("Klasse");
        syncRedisCommands.del("Vorname");
        syncRedisCommands.del("Nachname");
        syncRedisCommands.del("Notendurchschnitt");
        syncRedisCommands.del("Counter");
        syncRedisCommands.del("user:Happy");
        syncRedisCommands.lpush("mylist", "Hello");
    }

    @Before
    public void someCommandsToUseLaterInTests() {
        syncRedisCommands.set("Spengergasse", "4AHIF");
        Map<String, String> person = new HashMap<String, String>();
        person.put("Klasse", "4AHIF");
        person.put("Vorname", "Pascal");
        person.put("Nachname", "Happy");
        person.put("Notendurchschnitt", "4.0");
        syncRedisCommands.mset(person);
        syncRedisCommands.set("Counter", "12");
        Map<String, String> userdata = new HashMap<String, String>();
        userdata.put("Username", "Happy");
        userdata.put("Password", "ichbinbesonders");
        syncRedisCommands.hmset("user:Happy", userdata);
    }

    @Test
    public void setAValueToAKeyAndReturnTheKey() {
        Assert.assertEquals("4AHIF", syncRedisCommands.get("Spengergasse"));
    }

    @Test
    public void multisetValueKeysAndCheckIfTheyExist() {
        // using .get(0).getValue() to get the first value saved under the key and only return the value not the Key,Value format.
        Assert.assertEquals("4AHIF", syncRedisCommands.mget("Klasse").get(0).getValue());
        Assert.assertEquals("Pascal", syncRedisCommands.mget("Vorname").get(0).getValue());
        Assert.assertEquals("Happy", syncRedisCommands.mget("Nachname").get(0).getValue());
        Assert.assertEquals("4.0", syncRedisCommands.mget("Notendurchschnitt").get(0).getValue());
    }

    @Test
    public void popOneItemAndCheckIfTheFirstOneInsertedIsStillHere() {
        syncRedisCommands.lpop("mylist");
        Assert.assertFalse(syncRedisCommands.lrange("mylist", 0l, 0l).get(0).equals("World"));
        syncRedisCommands.lpush("mylist", "World");
    }


    @Test
    public void getHashMultiSetKeysWhichWereSetBefore() {
        Assert.assertEquals("Username", syncRedisCommands.hkeys("user:Happy").get(0));
        Assert.assertEquals("Password", syncRedisCommands.hkeys("user:Happy").get(1));
    }

    @Test
    public void getHashMultiSetValsWhichWereSetBefore() {
        Assert.assertThat(syncRedisCommands.hvals("user:talha2506").get(0), Is.is("Happy"));
        Assert.assertThat(syncRedisCommands.hvals("user:talha2506").get(1), Is.is("ichbinbesonders"));
    }

    @AfterClass
    public static void resetToStart() {
        redisClient.shutdown();
    }
}
